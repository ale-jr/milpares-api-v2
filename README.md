# API V2
Este e um guia de como consumir a nova API, aqui você consegue saber como consumir cada recurso :D

### [/accounts](/api/accounts/ACCOUNTS.md)
Gerenciamento das contas de login das lojas

## Loja Principal

### [/categories e /subcategories](/api/main_store/CATEGORIES.md)
Visualização das categorias e subcategorias dos modelos

### [/products]( /api/main_store/PRODUCTS.md)
Visualização dos produtos

### [/stock](/api/main_store/STOCK.md)
Visualização do estoque da **loja principal**

## Loja consignado

### [/orders](/api/orders/ORDERS.md)
Criação e visualização de pedidos para a loja principal

### [/sales](api/sales/SALES.md)
Efetuar, ver e cancelar vendas

### [/stock](api/stock/STOCK.md)
Ver o estoque atual
