#/sales
Ver, efetuar e estornar vendas

## Acessivel pela loja principal
### Ver todas as vendas
```
GET /adm/sales
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna
```json
{
    "_links": {
        "_self": "/adm/sales"
    },
    "items": [
        {
            "_links": {
                "_self": "/sales/1"
            },
            "id": 1,
            "loja": 7,
            "pagamento": 1,
            "modalidade": 1,
            "estornado": 1,
            "data": "2018-10-27T16:10:57.000Z"
        }
    ]
}
```


### Ver todas as vendas de uma loja
```
GET /adm/sales/:id_loja
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna
```json
{
    "_links": {
        "_self": "/adm/sales/7"
    },
    "items": [
        {
            "_links": {
                "_self": "/sales/1"
            },
            "id": 1,
            "loja": 7,
            "pagamento": 1,
            "modalidade": 1,
            "estornado": 1,
            "data": "2018-10-27T16:10:57.000Z"
        }
    ]
}
```

### Ver itens de uma venda
#### Com detalhes dos modelos
```
GET /adm/sales/items/:id_pedido?withProducts
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna

```json
{
    "_links": {
        "_self": "/adm/sales/items/1?withProducts"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "nome": "Sapatilha",
            "categoria": "Sapatilhas",
            "id_categoria": 1,
            "subcategoria": "Bico fino ",
            "id_subcategoria": 1,
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "modelo": 14,
            "tamanho": 38,
            "valor_unitario": 49.99,
            "quantidade": 50,
            "venda": 1,
            "estornado": 1
        }
    ]
}
```

#### Sem detalhes da venda

```
GET /adm/sales/items/:id_pedido
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna

```json
{
    "_links": {
        "_self": "/adm/sales/items/1"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "modelo": 14,
            "tamanho": 38,
            "valor_unitario": 49.99,
            "quantidade": 50,
            "venda": 1,
            "estornado": 1
        }
    ]
}

```


## Acessível pela loja consignada
### Ver todas as vendas da loja consignada
```
GET /sales
HEADERS
authorization : Bearer {JWT da loja consignada}
```
Retorna
```json
{
    "_links": {
        "_self": "/sales"
    },
    "items": [
        {
            "_links": {
                "_self": "/sales/1"
            },
            "id": 1,
            "loja": 7,
            "pagamento": 1,
            "modalidade": 1,
            "estornado": 1,
            "data": "2018-10-27T16:10:57.000Z"
        }
    ]
}

```

### Ver itens de uma venda
#### Com detalhes dos modelos
```
GET /sales/items/:id_pedido?withProducts
HEADERS
authorization : Bearer {JWT do consignado}
```
Retorna

```json
{
    "_links": {
        "_self": "/sales/items/1?withProducts"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "nome": "Sapatilha",
            "categoria": "Sapatilhas",
            "id_categoria": 1,
            "subcategoria": "Bico fino ",
            "id_subcategoria": 1,
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "modelo": 14,
            "tamanho": 38,
            "valor_unitario": 49.99,
            "quantidade": 50,
            "venda": 1,
            "estornado": 1
        }
    ]
}

```

#### Sem detalhes da venda

```
GET /sales/items/:id_pedido
HEADERS
authorization : Bearer {JWT do consignado}
```
Retorna

```json
{
    "_links": {
        "_self": "/sales/items/1"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "modelo": 14,
            "tamanho": 38,
            "valor_unitario": 49.99,
            "quantidade": 50,
            "venda": 1,
            "estornado": 1
        }
    ]
}

```

### Efetuar uma venda
Tipos de pagamento:
  * 1 :
  * 2 :
  * 3 :
  * 4 :
Tipos de venda:
  * 0 : Atacado
  * 1 : Varejo
  * 2 : Clube
  * 3 : Promoção

```
POST /sales
HEADERS
authorization : Bearer {JWT do consignado}
PAYLOAD
{
  "payment": 1,
  "mode": 1,
  "items": [
    {
      "modelo": 14,
      "tamanho": 38,
      "quantidade": 5
    }
  ]
}
```
Se der tudo certo, retorna um `204`
### Estornar uma venda
```
POST /sales/cancel/:sale_id
HEADERS
authorization : Bearer {JWT do consignado}
```
Se der certo, retorna um `204`
