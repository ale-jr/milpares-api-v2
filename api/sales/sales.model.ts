//variaveis de ambiente
import {environment} from '../../common/environment'

import {NotFoundError, NotAuthorizedError} from 'restify-errors'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'

import {Products} from '../main_store/products.model'

const name = "sales"
const table = "lojas_vendas"
const items_table = "lojas_vendas_itens"

const selectCount = () => database.getPool()(table)
.count('id')


//Campos do select com joi dos pedidos
const select_fields = ['lojas_pedidos.id as id','lojas.nome as nome_loja','lojas_vendas.*']

//Base para todos os selects de pedidos
const baseSelect = (fields) => database.getPool()(table)
.from(table)
.select(...fields)
.orderBy('lojas_vendas.id','desc')



//Select dos pedidos (com join ou não)
const selectAll = (join) => join == true ?
baseSelect(select_fields)
.join('lojas','lojas.id','lojas_vendas.loja')
:
baseSelect('*')


const select_barcode = 'LPAD(CONCAT(modelo,tamanho),6,"0") as `barcode`'


const select_fields_items = ['modelos.id','modelos.nome as nome','categorias.nome as categoria','categorias.id as id_categoria','subcategorias.nome as subcategoria','subcategorias.id as id_subcategoria','modelos.descricao','preco_catalogo','preco_1','preco_2','preco_3','producao','lojas_vendas_itens.*']


//Base para todos os selects de pedidos
const baseSelectItems = (fields) => database.getPool()(items_table)
.from(items_table)
.select(...fields)
.orderBy('lojas_vendas_itens.id','desc')


//Select dos itens (com join ou não)
const selectItems = (join) => join == true ?
baseSelectItems(select_fields_items)
.join('modelos','modelos.id','lojas_vendas_itens.modelo')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
:
baseSelectItems('*')


const ITEM_SCHEMA = joi.object().keys({
  modelo: joi.number(),
  tamanho: joi.number(),
  quantidade: joi.number()
})


const insertSale = (store_id,payment,mode,items) => {

    //se o item vier fora do array, coloca emum array
    if(!Array.isArray(items)) items = [items]
    //armazena algum erro de validação
    let validation_error = undefined
    //para cataitem, executa a validação
    items.forEach((item,index,array) => {
      const {error,value} = joi.validate(item,ITEM_SCHEMA,{presence: 'required'})
      if(error) validation_error = error
      Products.findById(value.modelo)
      .then(products => {
        if(products.length < 1) validation_error = new NotFoundError('modelo não encontrado')
        const product = products[0]
        let valor_unitario = product.preco_1
        switch(mode){
          case 0 : valor_unitario = product.preco_catalogo; break;
          case 1 : valor_unitario = product.preco_1; break;
          case 2 : valor_unitario = product.preco_2; break;
          case 3 : valor_unitario = product.preco_3; break;
        }
        array[index] = {...value, valor_unitario}
      })
      array[index] =  value
    })



    //Caso tenha ocorrido algum erro de validação
    if(validation_error) return Promise.reject(validation_error)

    //Tudo certo, volta uma promise com um transaction para adicionar a loja nas ordens e os itens nos itens da ordem
    return database.getPool()
    .transaction((trx) => trx
    .insert({loja:store_id,pagamento:payment,modalidade:mode},'id')
    .into(table)
    .then(ids =>
      trx.insert(
        items.map((value,index,array)=> ({...value, venda:ids[0]}))
      )
      .into(items_table))
    )
}

const cancelSale = (store_id,sale_id) =>
  database.getPool()(table)
  .select("*")
  .where({
    id: sale_id,
    loja: store_id
  })
  .then(sales => {
    if(sales.length < 1)  throw new NotAuthorizedError()
    database.getPool().transaction(trx => {
      database.getPool()(table)
     .update({estornado: true})
     .where('id',sale_id)
     .transacting(trx)
     .then( () =>
       database.getPool()(items_table)
      .update({estornado:true})
      .where({venda:sale_id})
     )
     .then(trx.commit)
     .catch(trx.rollback)
    })
  })


  const sumSales = (store_id,start,end) =>
    database.getPool()(table)
    .select('*')
    .select(database.getPool().raw('(SELECT SUM(lojas_vendas_itens.valor_unitario * lojas_vendas_itens.quantidade) FROM lojas_vendas_itens WHERE lojas_vendas_itens.venda = lojas_vendas.id) as total'))
    .where('estornado',0)
    .where('loja',store_id)
    .whereBetween('data',[start,end])
    .reduce((reduce,row)=> {
      console.log(row)
      switch(row.pagamento){
        //debito
        case 1:
        reduce.debit+= +row.total
        break;
        //crédito
        case 2:
        reduce.credit+= +row.total
        break;
        //dinheiro
        case 3:
        reduce.money+= +row.total
        break;
      }
      return reduce
    },{money:0,debt:0,credit:0})



export const Sales = {
  name,

  count: () => selectCount(),
  countWithStore: (store_id) => selectCount().where('loja',store_id),
  findAll: (join = false) => selectAll(join),

  findAllWithStore:(join = false,store_id) => selectAll(join).where('loja',store_id),

  findById: (id) => selectAll(true).where('lojas_vendas.id',id),

  findItems: (join = false, id) => selectItems(join).where('lojas_vendas_itens.venda',id),

  insert : (store_id,payment,mode,items) =>  insertSale(store_id,payment,mode,items),


  cancelSale : (store_id,sale_id) => cancelSale(store_id,sale_id),

  sumSales : (store_id,start,end) => sumSales(store_id,start,end)



}
