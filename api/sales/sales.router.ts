import {ModelRouter} from '../../common/model-router'
import {environment} from '../../common/environment'
import * as restify from 'restify'
import {NotFoundError, NotAuthorizedError} from 'restify-errors'
import {Sales} from './sales.model'
import {Stock} from '../stock/stock.model'


import {authorize,authorizeAdm} from '../../security/authz.handler'

class SalesRouter extends ModelRouter<Sales>{

  constructor(){
    super(Sales)
  }

  pageSize = 100

  //Sem id do vendedor
  findAll = this.selectSomething({
  join:true,
  id:false,
  count:Sales.count,
  select:Sales.findAll
  })

  findAllWithStore = this.selectSomething({
    join:true,
    id:true,
    count:Sales.countWithStore,
    select:Sales.findAllWithStore})

  findItems = this.selectSomething({
    join:true,
    id:true,
    count:undefined,
    select:Sales.findItems,
    envelope: {
    envelope:{ _self: false }
    }
  })


  createSale = (req,resp,next) => {
    const {items,payment,mode} = req.body
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    Sales.insert(store_id,payment,mode,items)
    .then(() => resp.send(204))
    .catch(next)
  }

  cancelSale = (req,resp,next) => {
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    const sale_id = req.params.sale_id
    Sales.cancelSale(store_id,sale_id)
    .then(() => resp.send(204))
    .catch(next)
  }


  applyRoutes(application: restify.Server){

    //Rotas do adm
    application.get(`/adm${this.basePath}`,this.findAll)
    application.get(`/adm${this.basePath}/:id`,this.findAllWithStore)
    application.get(`/adm${this.basePath}/items/:id`,this.findItems)


    //Rotas da loja consignada
    application.get(`${this.basePath}`,[authorize,this.findAllWithStore])
    application.get(`${this.basePath}/items/:id`,[this.findItems])

    application.post(`${this.basePath}`,[authorize,this.createSale])

    application.post(`${this.basePath}/cancel/:sale_id`,[authorize,this.cancelSale])

  }


}


export const salesRouter = new SalesRouter()
