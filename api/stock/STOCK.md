## /stock
Obtem e pesquisa no estoque da **loja do consignado**
* Usando token de loja consignada
  * [Obter todos itens em estoque](#obter-todos-itens-em-estoque)
  * [Obter item pelo id do modelo](obter-item-pelo-id-do-modelo)
  * [Buscar itens no estoque (com ou sem informações do odelo)](#buscar-itens-no-estoque)
* [Usando token da loja principal](#acessível-pela-loja-principal)

## Acessível pela loja consignada

### Obter todos itens em estoque
```
GET /stock
HEADERS
authorization : Bearer {Token gerado para a loja consignada}
```
Note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:
```json
{
    "_links": {
        "_self": "/stock",
        "next": "/stock?_page=2"
    },
    "items": [
        {
            "_links": {
                "_self": "/stock/6"
            },
            "id": 6,
            "loja": 10,
            "modelo": 14,
            "tamanho": 40,
            "quantidade": 10,
            "barcode": "001440"
        }
    ]
}
```
### Obter item pelo id do modelo

```
GET /stock/:product
HEADERS
authorization : Bearer {Token gerado para a loja consignada}
```
Retorna
```json
{
    "_links": {
        "_self": "/stock/14"
    },
    "items": [
        {
            "_links": {
                "_self": "/stock/5"
            },
            "id": 5,
            "id_modelo": 14,
            "categoria": "Sapatilhas",
            "subcategoria": "Bico fino ",
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "tamanho": 38,
            "quantidade": 10,
            "modelo": 14,
            "barcode": "001438"
        },
        {
            "_links": {
                "_self": "/stock/6"
            },
            "id": 6,
            "id_modelo": 14,
            "categoria": "Sapatilhas",
            "subcategoria": "Bico fino ",
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "tamanho": 40,
            "quantidade": 10,
            "modelo": 14,
            "barcode": "001440"
        }
    ]
}

```

### Buscar itens no estoque
#### Sem informações dos modelos
```
GET /stock/search
      ?id={id no estoque}
      &barcode={código de barras}
      &modelo={id do modelo}
      &tamanho={tamanho do modelo}
```
É necessário ao menos um parâmetro,note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:
```json
{
    "_links": {
        "_self": "/stock/search?barcode=001438"
    },
    "items": [
        {
            "_links": {
                "_self": "/stock/5"
            },
            "id": 5,
            "loja": 10,
            "modelo": 14,
            "tamanho": 38,
            "quantidade": 10,
            "barcode": "001438"
        }
    ]
}


```
#### Com informações dos modelos
Quando o parâmetro _withProducts_ é adicionado à URL, os itens em estoque serão populados com dados dos modelos, exemplo:
```
GET /main/stock/search
      ?withProducts
      &id={id no estoque}
      &barcode={código de barras}
      &modelo={id do modelo}
      &tamanho={tamanho do modelo}
      &subcategoria={subcategoria do modelo}
      &categoria={categoria do modelo}
      &preco_catalogo={preço de atacado do modelo}
      &preco_1={preço no varejo}
      &preco_2={preço no clube}
      &preco_3={preço promocional}
```
É necessário ao menos um parâmetro,note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:

```json
{
    "_links": {
        "_self": "/stock/search?withProducts&barcode=001438"
    },
    "items": [
        {
            "_links": {
                "_self": "/stock/5"
            },
            "id": 5,
            "id_modelo": 14,
            "categoria": "Sapatilhas",
            "subcategoria": "Bico fino ",
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "tamanho": 38,
            "quantidade": 10,
            "modelo": 14,
            "barcode": "001438"
        }
    ]
}

```



## Acessível pela loja principal

Para utilizar a API sem o token da loja consignada, basta usar o token da loja principal no HEADER (`authorizationAdm : Bearer {JWT de admin}`) e mudar as urls para as urls indicadas à baixo, passando o id da loja a ser buscada
* `GET /stock` > `GET /adm/stock/:id_da_loja`
* `GET /adm/stock/:product` > `GET /adm/stock/:id_da_loja/:product`
* `GET /stock/search` > `GET /adm/stock/:id_da_loja/search`
