import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Stock} from './stock.model'

import {authorize,authorizeAdm} from '../../security/authz.handler'


class StockRouter extends ModelRouter<Stock>{

  constructor(){
    super(Stock)
  }

  pageSize = 1

  findByParams = (req,resp,next) => {
    const {query} = req
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    let page = parseInt(query._page || 1)
    page = page > 0 ? page : 1

    const skip = (page -1) * this.pageSize

    const join = query.withProducts != undefined ? true : false
    delete query.withProducts
    Stock.countWhere(query,store_id,join)
    .then(count => Stock.findByParams(query,store_id,join)
    .limit(this.pageSize)
    .offset(skip)
    .then(this.renderAll(resp,next,{
      page,count,pageSize: this.pageSize,url: req.url, resource: '/search'
    })))
    .catch(next)
  }

  findAll = (req,resp,next) => {
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    console.log('store_id',store_id)
    let page = parseInt(req.query._page || 1)
    page = page > 0 ? page : 1

    const skip = (page -1) * this.pageSize


    const join = req.query.withProducts != undefined ? true : false

    Stock.count(store_id,join)
    .then(count=> Stock.findAll(store_id,join)
    .limit(this.pageSize)
    .offset(skip)
    .then(this.renderAll(resp,next,{
      page,count, pageSize: this.pageSize,url: req.url
    })))
    .catch(next)
  }


  findByProduct = (req,resp,next) =>{
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    console.log('store',store_id)
    this.model.findByProduct(req.params.product,store_id)
    .then(this.renderAll(resp,next,{url: req.url}))
    .catch(next)
  }



  applyRoutes(application: restify.Server){
    //Do Admin
    application.get(`${this.basePath}/adm/:id`,[authorizeAdm,this.findAll])
    application.get(`${this.basePath}/adm/:id/:product`,[authorizeAdm,this.findByProduct])
    application.get(`${this.basePath}/adm/:id/search`,[authorizeAdm,this.findByParams])

    //Do usuário
    application.get(`${this.basePath}`,[authorize,this.findAll])
    application.get(`${this.basePath}/:product`,[authorize,this.findByProduct])
    application.get(`${this.basePath}/search`,[authorize,this.findByParams])
  }
}


export const stockRouter = new StockRouter()
