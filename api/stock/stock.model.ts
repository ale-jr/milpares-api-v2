//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'


const name : string = 'stock'
const table : string = 'lojas_estoque'
const table_out : string  = 'lojas_saida'
const table_in : string = 'lojas_entrada'
/*
CREATE TABLE lojas_estoque (
id int not null auto_increment primary key,
loja int not null,
modelo int not null,
tamanho int not null,
quantidade int null default 0
);
*/

export interface Document{
  id?:number,
  loja?:number | object,
  modelo?: number | object,
  tamanho?: number,
  quantidade?: number
}


const select_barcode = 'LPAD(CONCAT(modelo,tamanho),6,"0") as `barcode`'
const select_fields = ['lojas_estoque.id as id','modelos.id as id_modelo','categorias.nome as categoria','subcategorias.nome as subcategoria','modelos.descricao','preco_catalogo','preco_1','preco_2','preco_3','producao','tamanho','quantidade','modelo',]


const SEARCH_PARAMS = joi.object().keys({
  'lojas_estoque.id': joi.number(),
  modelo: joi.number(),
  tamanho : joi.number(),
  subcategoria: joi.number(),
  'categorias.id': joi.number(),
  preco_catalogo: joi.number(),
  preco_1: joi.number(),
  preco_2 : joi.number(),
  preco_3: joi.number(),
})
const SEARCH_PARAMS_NO_JOIN = joi.object().keys({
  'lojas_estoque.id': joi.number(),
  modelo: joi.number(),
  tamanho : joi.number(),
})


const selectAll = (id,join = false) => join == true ?  database.getPool()
.select(...select_fields)
.select(database.getPool().raw(select_barcode))
.from(table)
.join('modelos','lojas_estoque.modelo','modelos.id')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
.where('loja',id)
.orderBy('modelos.id','desc')
:
database.getPool()
.select('*')
.select(database.getPool().raw(select_barcode))
.from(table)
.where('loja',id)
.orderBy('id','desc')


const selectCount = (id,join=false) => join ?
database.getPool()(table)
.count('modelos.id')
.where('loja',id)
.join('modelos','lojas_estoque.modelo','modelos.id')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
.where('loja',id)
:
database.getPool()(table)
.count('id')
.where('loja',id)


const findWhere = (where,select,join = false) => {
  delete where._page
  if(where.barcode){
    where.modelo = where.barcode.slice(0,-2)
    where.tamanho = where.barcode.slice(-2)
    delete where.barcode
  }
  if(where.id) {
    where['lojas_estoque.id'] = where.id
    delete where.id
  }
  if(where.categoria){
    where['categorias.id'] = where.categoria
    delete where.categoria
  }
  const search_params = join ? SEARCH_PARAMS : SEARCH_PARAMS_NO_JOIN

  const {error,value} = joi.validate(where,search_params, {presence: 'optional'})
  if(error) return Promise.reject(error)
  return select.where({...value})
}



const STOCK_ITEM = joi.object().keys({
  modelo: joi.number(),
  tamanho: joi.number(),
  quantidade: joi.number(),
  loja: joi.number(),
  valor_unitario:joi.number().optional(),
  modo: joi.number().optional()
})


//inserir no estoque
const stockEntry = (items,table) => {
if(!Array.isArray(items)) items = [items]
let validation_error = undefined
const values = items.map((item,index,array)=> {
  const {error,value} = joi.validate(item,STOCK_ITEM,{presence: 'required'})
  if(error) validation_error = error
  return value
})


  if(validation_error) return Promise.reject(validation_error)
  return database.getPool()(table).insert(values)
}

export const Stock = {
  name,

  //Obter apenas a loja do cliente
  count: (store_id,join) => selectCount(store_id,join),
  countWhere: (params,store_id,join) => findWhere(params,selectCount(store_id,join),join),

  findByParams: (params,store_id,join = false) => findWhere(params,selectAll(store_id,join),join),

  //Obter todos da loja selecionada
  findAll: (store_id,join) => selectAll(store_id,join),

  //Obter por modelo
  findByProduct : (product_id,store_id) => findWhere({'modelo':product_id},selectAll(store_id,true)),

  //insert int stock
  stockInsert : (item) => stockEntry(item,table_in),
  stockRemove : (item) => stockEntry(item,table_out),



}
