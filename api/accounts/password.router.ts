import {ModelRouter} from '../../common/model-router'
import {environment} from '../../common/environment'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Passwords} from './password.model'
import {Accounts} from './accounts.model'
import {newPassword} from '../../email/stores'

class PasswordsRouter extends ModelRouter<Passwords>{


  constructor(){
    super(Passwords)
  }



  findByToken = (req,resp,next) => {
    Passwords.findByToken(req.params.token)
    .then(details => {
      if(details.length < 1) throw new NotFoundError('Token Inválido')
      details = details[0]

      let token_timeout = +details.data + +environment.security.token_timeout
      if(token_timeout < Date.now() || details.valido !== 1 ){
        throw new NotFoundError('Token Inválido')
      }
      delete details.hash
      super.render(resp,next)(details)
    })
    .catch(next)
  }

  newToken = (req, resp, next) => {
    Passwords.createToken(req.body.email)
    .then(() => resp.send(200))
    .catch(next)
  }

  changePassword = (req,resp,next) => {
    const token = req.params.token
    Passwords.findByToken(token)
    .then(details => {
      if(details.length < 1) throw new NotFoundError('Token Inválido')
      details = details[0]
      return Accounts.update(details.id,(<any>{senha:req.body.password}))
    })
    .then(() => Passwords.deactivateToken(token))
    .then(()=> resp.send(200))
    .catch(next)
  }



  applyRoutes(application: restify.Server){
    application.get(`${this.basePath}/password/verify/:token`,this.findByToken)
    application.post(`${this.basePath}/password/new`,this.newToken)
    application.post(`${this.basePath}/password/change/:token`,this.changePassword)

  }
}

export const passwordsRouter = new PasswordsRouter()
