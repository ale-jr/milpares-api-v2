import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Accounts} from './accounts.model'
import {authenticate} from '../../security/auth.handler'
import {authorize,authorizeAdm} from '../../security/authz.handler'

class AccountsRouter extends ModelRouter<Accounts>{

  constructor(){
    super(Accounts)
  }

  verify = (req,resp,next) => resp.send(200)

  //Evita com que dados não editáveis entrem na requisição
  sanitizeData = (req,resp,next) => {
      if(req.body){
        delete req.body.id
        delete req.body.ativo
        delete req.body.criacao
      }
  }

  applyRoutes(application: restify.Server){

      //Só o adm pode
      application.get(`${this.basePath}/adm`,[authorizeAdm,this.findAll])
      application.get(`${this.basePath}/adm/:id`,[authorizeAdm,this.findById])
      application.patch(`${this.basePath}/adm/:id`,[authorizeAdm,this.update])
      application.post(`${this.basePath}/adm`,[authorizeAdm,this.save])
      application.del(`${this.basePath}/adm/:id`,[authorizeAdm,this.delete])

      //Só a própria loja pode
      application.get(`${this.basePath}`,[authorize,this.findById])
      application.patch(`${this.basePath}`,[authorize,this.sanitizeData,this.update])

      //Autenticação
      application.post(`${this.basePath}/authenticate`,authenticate)
      application.get(`${this.basePath}/verify`,[authorize,this.verify])
  }
}


export const accountsRouter = new AccountsRouter()
