//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'

//geração de hash
import * as bcrypt from 'bcrypt'
import {randomBytes} from 'crypto'


import {Accounts} from './accounts.model'

import {newPassword} from '../../email/stores'

/*
CREATE TABLE token_lojas (
id int not null primary key auto_increment,
token varchar(200) not null,
data timestamp not null default current_timestamp,
loja int not null,
valido boolean not null default true,
FOREIGN KEY (loja) REFERENCES lojas(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);
*/

const name: string = 'accounts'
const table : string = 'token_lojas'

export interface Document {
  id : number,
  token: string,
  data: number | Date,
  loja: number,
  valido: boolean | number
}

export const Passwords = {
  name,
  //obter detalhes do token pelo token
  findByToken: (token) => database.getPool()
  .select('*')
  .from(table)
  .join('lojas','lojas.id','token_lojas.loja')
  .where('token',token),

  //desativar token
  deactivateToken: (token) => database.getPool()(table)
  .where('token',token)
  .update({valido:0}),

  //cria um token para definir uma nova senha
  createToken: (email) => database.getPool()
  .select('id','nome')
  .from('lojas')
  .where('email',email)
  .then(results => {
    if(results.length < 1) return Promise.resolve(false)
    const {nome,id} = results[0]
    const token = randomBytes(48).toString('hex')
    return database.getPool()(table)
    .insert({
      loja: id,
      token})
    .then(results => newPassword(email,nome,token))
  })

}
