//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'

//geração de hash
import * as bcrypt from 'bcrypt'


const name : string = 'accounts'
const table : string = 'lojas'

export interface Document{
  id?: number,
  nome: string,
  email: string,//max 50
  hash?: string,
  senha?: string,
  ativo?: number | boolean,
  criacao?:  number | Date
}

const SCHEMA = joi.object().keys({
  id: joi.number().optional(),
  nome: joi.string().max(100), //será obrigatório quando presence for required
  email: joi.string().email(),
  senha: joi.string().min(8).optional(), //será opcional quando presence for required
  ativo: joi.number().optional()
})


const hashPassword =  (document:Document) => {


      if(!document.senha){
        let unhashed_document = Object.assign(document)
        delete unhashed_document.senha
        return unhashed_document
      }

      let hash = bcrypt.hashSync(document.senha,environment.security.saltRounds)
      let hashed_document = Object.assign({hash},document)
      delete hashed_document.senha
      return hashed_document
}

export const Accounts = {

  name,

  //Count
  count: () => database.getPool()(table)
  .count('id'),



  //Find all
  findAll: () => database.getPool()
  .select('id','nome', 'email', 'ativo', 'criacao')
  .from(table),



  //FindById
  findById: (id) => database.getPool()
  .select('id','nome', 'email', 'ativo', 'criacao')
  .from(table)
  .where('id',id),


  //FindByEmail
  findByEmail: (email: string) => database.getPool()
  .select('id','nome', 'email', 'ativo', 'criacao','hash')
  .from(table)
  .where('email',email),


  //VerifyPassword
  passwordMatches: (user, password) => {
  if(Array.isArray(user)) user = user[0]
  return bcrypt.compareSync(password,user.hash)
},



  //Save
  save: (document: Document) =>{
  const {error,value} = joi.validate(document,SCHEMA,{ presence: 'required' })
  if(error) return Promise.reject(error)
  const hashed_document = hashPassword(value)
  return database.getPool()(table)
  .insert(hashed_document)
  },



  //Update
  update: (id: number, document:Document) =>{
  const {error,value} = joi.validate(document,SCHEMA,{presence: 'optional'})
  if(error) return Promise.reject(error)
  const hashed_document = hashPassword(value)
  return database.getPool()(table)
  .where('id',id)
  .update(hashed_document)
  },



  //Delete
  remove: (id:number) => database.getPool()(table)
  .where('id',id)
  .del(),



}
