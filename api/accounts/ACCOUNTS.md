## /accounts
onde você pode criar, editar e deletar contas das lojas, bem como gerar o token de autenticação

* Acessiveis com o token de administrador
    * [Obter todas lojas](#obter-todas-lojas)
    * [Obter loja por id](#lojas-por-id)
    * [Atualizar loja](#atualizar-loja)
    * [Nova loja](#nova-loja)
    * [Deletar uma loja](#deletar-uma-loja)
* Acessiveis pela loja
    * [Obter token de acesso](#obter-token-de-acesso)
    * [Verificar o token de acesso](#verifica-o-token-de-acesso)
    * [Obter dados da loja](#obter-dados-da-loja)
    * [Editar dados da loja](#editar-dados-da-loja)
    * [Criar nova senha](#criar-nova-senha)
    * [Verificar Token para nova senha](#verificar-token-para-nova-senha)
    * [Alterar senha](#alterar-senha)

### Acessiveis com o token de ADM
#### Obter todas lojas
```
GET /accounts/adm
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Obtem todos os dados de todas as lojas,retorna algo como:

````
{
"_links":{
  "_self": "/accounts/adm"
},
"items":[
  {
  "_links":{
    "_self": "/accounts/adm/6"
  },
  "id": 6,
  "nome": "Loja 1",
  "email": "loja1@gmail.com",
  "ativo": 1,
  "criacao": "2018-10-17T23:15:14.000Z"
  }
]
}
````

#### Lojas por id

````
GET /accounts/adm/:id
HEADERS
authorizationAdm : Bearer {JWT de admin}
````
Obtem dados de uma loja em específico, de acordo com seu id, retorna

```
{
"_links":{
"_self": "/accounts/adm/6"
},
"id": 6,
"nome": "Loja 1",
"email": "loja1@gmail.com",
"ativo": 1,
"criacao": "2018-10-17T23:15:14.000Z"
}
```

#### Atualizar loja
```
PATCH /accounts/adm/:id
HEADERS
authorizationAdm : Bearer {JWT de admin}


PAYLOAD
{
  "nome": "Loja editada 1",
  "email": "loja1@hotmail.com",
  "senha": "senhaSecreta",
  "ativo": 1
}

```
Atualiza os dados da loja pelo id na url, é necessário ao menos um item descrito no payload, são retornados os dados atualizados da loja

```
{
  "id": 10,
  "nome": "Loja editada 1",
  "email": "loja1@hotmail.com",
  "senha": "senhaSecreta",
  "ativo": 1
}

```

#### Nova loja

```
POST /accounts/adm
HEADERS
authorizationAdm : Bearer {JWT de admin}


PAYLOAD
{
  "nome": "Loja editada 1",
  "email": "loja1@hotmail.com",
  "senha": "senhaSecreta",
  "ativo": 1
}

```

Cria nova loja, sendo necessário ao menos um nome e email no payload, caso a senha não seja definida, será enviado um email para a loja com um link de criação de senha. São retornados os dados da criação da nova loja

```
{
"_links":{
"_self": "/accounts//8"
},
"id": 8,
"nome": "Nova loja",
"email": "loja2@hotmail.com",
"ativo": 1,
"criacao": "2018-10-27T16:50:46.000Z"
}
```

#### Deletar uma loja

```
DELETE /accounts/adm/:id
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Deleta a loja descrita no id, o corpo da requisição volta vazio e com código 204


### Acessiveis pela loja

#### Obter token de acesso
```
POST /accounts/authenticate

PAYLOAD
{
  "email": "loja1@hotmail.com",
  "password": "senha1234"
}
```
Irá obter o token JWT para todas as futuras requisições, retornando
```
{
"nome": "Loja editada 1",
"email": "loja1@hotmail.com",
"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJsb2phMUBob3RtYWlsLmNvbSIsImlzcyI6InN0b3Jlcy1hcGkiLCJpYXQiOjE1NDA2NTk0NzZ9.MDBKdnSNK0ehvjT4EBFa5dd0cu323dQerYgSpxVvAnQ"
}
```

Caso email ou senha incorretos, retorna um 403

#### Verifica o token de acesso
```
GET /accounts/verify
authorization : Bearer {JWT de admin}
```
Verifica se o token é valido, casoseja válido é retornado um 200, do contrário, um 403


#### Obter dados da loja
```
GET /accounts
authorization : Bearer {JWT de admin}
```
Retorna
```
  {
  "_links":{
    "_self": "/accounts//10"
  },
  "id": 10,
  "nome": "Loja editada 1",
  "email": "loja1@hotmail.com",
  "ativo": 1,
  "criacao": "2018-10-17T23:15:14.000Z"
  }
```


#### Editar dados da loja

```
PATCH /accounts
HEADERS
authorization : Bearer {JWT de admin}

PAYLOAD
{
  "nome": "Loja 2",
  "email": "loja1@hotmail.com",
  "senha": "senha1234"
}
```
Necessário ao menos um dos itens descritos no PAYLOAD, retorna todos os dados atualizados

```
  {
  "_links":{
    "_self": "/accounts//10"
  },
  "id": 10,
  "nome": "Loja 2",
  "email": "loja1@hotmail.com",
  "ativo": 1,
  "criacao": "2018-10-17T23:15:14.000Z"
  }
```

#### Criar nova senha

```
POST /accounts/password/new

PAYLOAD
{
  "email": ""
}
```
Se tudo der certo, irá retornar apenas código 200, mesmo se o email não existir na base de dados

#### Verificar Token para nova senha
```
POST /accounts/password/verify/:token
```
Obtem informações sobre um token, caso o token exista, retorna
```
  {
  "_links":{
  "_self": "/accounts/10"
  },
  "id": 10,
  "token": "082aa29c41bdb1c30585073e6e61e61c6360a99a07fc7e87b724b5af5dd29926a349512ccc8558f8bb682fd3b1f3cba1",
  "data": "2018-10-27T18:06:26.000Z",
  "loja": 10,
  "valido": 1,
  "nome": "Loja 2",
  "email": "loja1@hotmail.com",
  "ativo": 1,
  "criacao": "2018-10-17T23:15:14.000Z"
  }
```
Caso contrário, volta um 404

#### Alterar senha
```
POST /accounts/password/change/:token

PAYLOAD
{
  "password": "shhhhhh"
}
```
Altera a senha de acordo com o token fornecido,em caso de sucesso retorna apenas um código 200
