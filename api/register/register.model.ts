//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'

export const name = 'register'
const table = 'lojas_caixa'


export const selectLast = (store_id) =>
  database.getPool()
  .from(table)
  .where('loja',store_id)
  .orderBy('id','DESC')
  .limit(1)

export const isOpen = (store_id) =>
  selectLast(store_id)
  .then(items => {
      if(items.length < 1) return false
      if(items[0].data_fechamento == null) return items[0]

      return false
  })

export const open = (store_id,money) =>
  database.getPool()(table)
  .insert({
    loja: store_id,
    dinheiro_abertura: money
  })

const whereRegister = (store_id) =>
database.getPool()(table)
.where('loja',store_id)
.whereNull('data_fechamento')


const update = (store_id,fields) =>
  whereRegister(store_id)
  .update(fields)




export const close = (store_id,money,credit,debt) =>
  update(store_id,{
    data_fechamento : database.getPool().fn.now(),
    dinheiro_fechamento: money,
    credito: credit,
    debito: debt
  })


export const drain = (store_id,money) => update(store_id,{sangria: money})

export const findAll = (store_id) =>
  database.getPool()
  .from(table)
  .where('loja',store_id)

export const count = (store_id) =>
  database.getPool()
  .from(table)
  .count('id')
  .where('loja',store_id)

export const isMoney = (money) => !isNaN(money)
