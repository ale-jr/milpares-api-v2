import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError, BadRequestError, ConflictError} from 'restify-errors'
import * as Register from './register.model'
import {authenticate} from '../../security/auth.handler'
import {authorize} from '../../security/authz.handler'

import {Sales} from '../sales/sales.model'

class RegisterRouter extends ModelRouter<Register>{
  constructor(){
    super(Register)
  }

  open = (req,resp,next) => {
    const store_id = req.params.id


    Register.isOpen(store_id)
    .then(isOpen => {
      if(!req.body) throw new BadRequestError('money attribute is required')
      //208 Already Reported
      if(isOpen) return resp.send(208)


      console.log('é money',Register.isMoney(req.body.money))

      //filtar se money é money
      if(!Register.isMoney(req.body.money)) throw new BadRequestError('money attribute has to be a number')

      Register.open(store_id,req.body.money)
      .then(() => resp.send(204))
      .catch(next)
    })
    .catch(next)
  }

  drain = (req,resp,next) => {
    const store_id = req.params.id


    Register.isOpen(store_id)
    .then(isOpen => {
      //Tratando erros
      if(!req.body) throw new BadRequestError('money attribute is required')

      if(!isOpen) throw new ConflictError('register is not open')

      if(!Register.isMoney(req.body.money)) throw new BadRequestError('money attribute has to be a number')
      //Todos erros tratados, prosseguir com a sangria
      const money = +req.body.money + isOpen.sangria
      return Register.drain(store_id,money)
      .then(() => resp.send(204))
    })
    .catch(next)
  }


  close = (req,resp,next) => {
    const store_id = req.params.id
    Register.isOpen(store_id)
    .then(isOpen => {
      if(!req.body) throw new BadRequestError('money attribute is required')

      if(!isOpen) throw new ConflictError('register is not open')

      if(!Register.isMoney(req.body.money)) throw new BadRequestError('money attribute has to be a number')

      const {data_abertura,sangria,dinheiro_abertura} = isOpen
      const data_fechamento = new Date()

      return Sales.sumSales(store_id,data_abertura,data_fechamento)
      .then(({money,debt,credit}) => {
          console.log('money',money)
          const sold = (+req.body.money + sangria) - dinheiro_abertura

          const balance = sold - money

          return Register.close(store_id,money,credit,debt)
          .then(() => resp.json({balance}))
      })
    })
    .catch(next)
  }

  findAll = this.selectSomething({
    join: false,
    id:true,
    count: Register.count,
    select: Register.findAll,
    envelope:{
      envelope:{
         _self: false
       }
    },
  })

  applyRoutes(application: restify.Server){
      application.post(`${this.basePath}/open`,[authorize,this.open])

      application.post(`${this.basePath}/drain`,[authorize,this.drain])

      application.post(`${this.basePath}/close`,[authorize,this.close])

      application.get(`${this.basePath}`,[authorize,this.findAll])

  }

}

export const registerRouter = new RegisterRouter()
