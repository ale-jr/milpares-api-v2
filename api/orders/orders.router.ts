import {ModelRouter} from '../../common/model-router'
import {environment} from '../../common/environment'
import * as restify from 'restify'
import {NotFoundError, NotAuthorizedError} from 'restify-errors'
import {Orders} from './orders.model'
import {Stock} from '../stock/stock.model'

import {authorize,authorizeAdm} from '../../security/authz.handler'

class OrdersRouter extends ModelRouter<Orders>{

  constructor(){
    super(Orders)
  }

  pageSize = 100

  //Sem id do vendedor
  findAll = this.selectSomething({
    join: true,
    id:false,
    count: Orders.count,
    select: Orders.findAll,
  })

  findAllWithStore = this.selectSomething({
    join:true,
    id:true,
    count: Orders.countWithStore,
    select: Orders.findAllWithStore
  })

  findItems = this.selectSomething({
    join:true,
    id:true,
    count:undefined,
    select:Orders.findItems,
    envelope:{
      envelope:{
         _self: false
       }
    },
    id_field: 'order_id'
  })


  createOrder = (req,resp,next) => {
    const items = req.body
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    Orders.insert(store_id,items)
    .then(inserts => resp.send(204))
    .catch(next)
  }

  finishOrder = (req,resp,next) => {
    const store_id = req.params.id //TODO: puxar lá do req.authenticated
    const order_id = req.params.order
    const items = req.body
    Orders.findById(order_id)
    .where('finalizado',0)
    .then(details => {
      if(details.length < 1) throw new NotFoundError('Ordem não encontrada ou finalizada')
      if(details[0].id_loja != store_id) throw new NotAuthorizedError()
      return true
    })
    .then(() => Orders.finishOrder(order_id))
    .then(()=> Stock.stockInsert(
      items.map((item,index,arr) => ({...item, loja:store_id, modo: 2
      }))
    ))
    .then(() => resp.send(200))
    .catch(next)
  }

  applyRoutes(application: restify.Server){

    //Com token de ADM
    //TODO: colocar authorize do admin
    application.get(`/adm${this.basePath}`,this.findAll)
    application.get(`/adm${this.basePath}/:id`,this.findAllWithStore)
    application.get(`/adm${this.basePath}/items/:order_id`,this.findItems)
    application.post(`/adm${this.basePath}/finish/:id/:order`,this.finishOrder)

    //pela loja consignada
    application.get(`${this.basePath}`,[authorize,this.findAllWithStore])
    application.get(`${this.basePath}/items/:order_id`,[authorize,this.findItems])
    application.post(`${this.basePath}`,[authorize,this.createOrder])

  }


}


export const ordersRouter = new OrdersRouter()
