#/orders
Obter, criar e finalizar pedidos de transferência de estoque da loja principal para a loja consignada

* [Acessivel pela loja principal](#acessivel-pela-loja-principal)
  * [Ver todos os pedidos](#ver-todos-os-pedidos)
  * [Ver todos pedidos pelo id da loja consignada](#ver-todos-pedidos-pelo-id-da-loja-consignada)
  * [Ver itens de um pedido de transferência](#ver-itens-de-um-pedido-de-transferência)
  * [Confirmar transferência de itens](#confirmar-transferência-de-itens)
* [Acessível pela loja consignada](#acessível-pela-loja-consignada)
  * [Ver todos pedidos da loja consignada](#ver-todos-pedidos-da-loja-consignada)
  * [Ver itens de um pedido de transferência](#ver-itens-de-um-pedido-de-transferência)
  * [Fazer um pedido](#fazer-um-pedido)

## Acessivel pela loja principal

### Ver todos os pedidos
```
GET /adm/orders
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna

```json
{
    "_links": {
        "_self": "/adm/orders"
    },
    "items": [
        {
            "_links": {
                "_self": "/orders/5"
            },
            "id": 5,
            "loja": 10,
            "data": "2018-10-24T21:44:19.000Z",
            "finalizado": 1
        }
    ]
}

```

### Ver todos pedidos pelo id da loja consignada
```
GET /adm/orders/:id
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:

```json
{
    "_links": {
        "_self": "/adm/orders/10"
    },
    "items": [
        {
            "_links": {
                "_self": "/orders/5"
            },
            "id": 5,
            "loja": 10,
            "data": "2018-10-24T21:44:19.000Z",
            "finalizado": 1
        }
    ]
}

```

### Ver itens de um pedido de transferência

####Com detalhes dos produtos
```
GET /adm/orders/items/:id_pedido?withProducts
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna:
```json
{
    "_links": {
        "_self": "/adm/orders/items/5?withProducts"
    },
    "items": [
        {
            "_links": {
            },
            "id": 14,
            "nome": "Sapatilha",
            "categoria": "Sapatilhas",
            "id_categoria": 1,
            "subcategoria": "Bico fino ",
            "id_subcategoria": 1,
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "tamanho": 38,
            "quantidade": 10
        }
    ]
}

```

#### Sem detalhes dos produtos
```
GET /adm/orders/items/:id_pedido
HEADERS
authorizationAdm : Bearer {JWT de admin}
```
Retorna:
```json
{
    "_links": {
        "_self": "/adm/orders/items/5"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "pedido": 5,
            "modelo": 14,
            "tamanho": 38,
            "quantidade": 10
        }
    ]
}

```

### Confirmar transferência de itens
```
POST /adm/orders/finish/:id_loja/:order
HEADERS
authorizationAdm : Bearer {JWT de admin}
PAYLOAD
[
  {
    "modelo": id_modelo,
    "quantidade": quantidade,
    "tamanho": tamanho
  }, ...
]
```
Retorna código `200` e caso de sucesso ou `404` se ordem não encontrada

## Acessível pela loja consignada

### Ver todos pedidos da loja consignada
```
GET /orders
HEADERS
authorization : Bearer {Token da loja consignada}
```
Note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:

```json
{
    "_links": {
        "_self": "/orders"
    },
    "items": [
        {
            "_links": {
                "_self": "/orders/5"
            },
            "id": 5,
            "loja": 10,
            "data": "2018-10-24T21:44:19.000Z",
            "finalizado": 1
        }
    ]
}
```

### Ver itens de um pedido de transferência

####Com detelhes dos produtos
```
GET /orders/items/:id_pedido?withProducts
HEADERS
authorization : Bearer {Token gerado para a loja consignada}
```
Retorna:

```json
{
    "_links": {
        "_self": "/orders/items/5?withProducts"
    },
    "items": [
        {
            "_links": {
            },
            "id": 14,
            "nome": "Sapatilha",
            "categoria": "Sapatilhas",
            "id_categoria": 1,
            "subcategoria": "Bico fino ",
            "id_subcategoria": 1,
            "descricao": null,
            "preco_catalogo": 29.99,
            "preco_1": 49.99,
            "preco_2": 39.99,
            "preco_3": 99.99,
            "producao": "Jeans com desenho do Mikey ",
            "tamanho": 38,
            "quantidade": 10
        }
    ]
}
```

####Sem detelhes dos produtos
```
GET /orders/items/:id_pedido
HEADERS
authorization : Bearer {Token gerado para a loja consignada}
```
Retorna:

```json
{
    "_links": {
        "_self": "/orders/items/5"
    },
    "items": [
        {
            "_links": {
            },
            "id": 1,
            "pedido": 5,
            "modelo": 14,
            "tamanho": 38,
            "quantidade": 10
        }
    ]
}

```

### Fazer um pedido
```
POST /orders
HEADERS
authorization : Bearer {Token gerado para a loja consignada}
PAYLOAD
[
  {
    "modelo": id_modelo,
    "tamanho": tamanho,
    "quantidade": quantidade
  }
]
```
Retorna um `204` se deu tudo certo
