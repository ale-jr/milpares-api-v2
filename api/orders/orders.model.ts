//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos
import * as joi from 'joi'

const name = "orders"
const table = "lojas_pedidos"
const items_table = "lojas_pedidos_itens"


//Select ara fazer a contagem dos itens
const selectCount = () => database.getPool()(table)
.count('id')

//Campos do select com joi dos pedidos
const select_fields = ['lojas_pedidos.id as id','lojas.nome as loja','lojas.id as id_loja','data','finalizado']


//Base para todos os selects de pedidos
const baseSelect = (fields) => database.getPool()(table)
.from(table)
.select(...fields)
.orderBy('lojas_pedidos.id','desc')

//Select dos pedidos (com join ou não)
const selectAll = (join) => join == true ?
baseSelect(select_fields)
.join('lojas','lojas.id','lojas_pedidos.loja')
:
baseSelect('*')


const select_barcode = 'LPAD(CONCAT(modelo,tamanho),6,"0") as `barcode`'

const select_fields_items = ['modelos.id','modelos.nome as nome','categorias.nome as categoria','categorias.id as id_categoria','subcategorias.nome as subcategoria','subcategorias.id as id_subcategoria','modelos.descricao','preco_catalogo','preco_1','preco_2','preco_3','producao','tamanho','quantidade']


//Base para todos os selects de pedidos
const baseSelectItems = (fields) => database.getPool()(items_table)
.from(items_table)
.select(...fields)
.orderBy('modelo','desc')

//Select dos itens (com join ou não)
const selectItems = (join) => join == true ?
baseSelectItems(select_fields_items)
.join('modelos','modelos.id','lojas_pedidos_itens.modelo')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
:
baseSelectItems('*')


const ITEM_SCHEMA = joi.object().keys({
  modelo: joi.number(),
  tamanho: joi.number(),
  quantidade: joi.number()
})

const insertOrder = (store_id,items) => {

    //se o item vier fora do array, coloca emum array
    if(!Array.isArray(items)) items = [items]
    //armazena algum erro de validação
    let validation_error = undefined
    //para cataitem, executa a validação
    let values = items.map((item,index,array) => {
      const {error,value} = joi.validate(item,ITEM_SCHEMA,{presence: 'required'})
      if(error) validation_error = error
      return value
    })



    //Caso tenha ocorrido algum erro de validação
    if(validation_error) return Promise.reject(validation_error)

    //Tudo certo, volta uma promise com um transaction para adicionar a loja nas ordens e os itens nos itens da ordem
    return database.getPool()
    .transaction((trx) => trx
    .insert({loja:store_id},'id')
    .into(table)
    .then(ids =>
      trx.insert(
        values.map((value,index,array)=> ({...value, pedido:ids[0]}))
      )
      .into(items_table))
    )
}


//Ações
export const Orders = {
  name,

  count: () => selectCount(),
  countWithStore: (store_id) =>selectCount().where('loja',store_id),


  findAll: (join = false) => selectAll(join),

  findAllWithStore:(join = false,store_id) => selectAll(join).where('loja',store_id),

  findById: (id) => selectAll(true).where('lojas_pedidos.id',id),

  findItems: (join = false, id) => selectItems(join).where('lojas_pedidos_itens.pedido',id),

  insert : (store_id,items) =>  insertOrder(store_id,items),

  finishOrder : (order_id) => database.getPool()(table)
  .where('lojas_pedidos.id',order_id).update({finalizado:1})
}
