## /stock
Obtem e pesquisa no estoque da **loja principal**
* [Obter todos os itens em estoque (com e sem informações do modelo)](#obter-todos-os-itens-em-estoque)
* [Obter item em estoque pelo id](#obter-item-em-estoque-pelo-id)
* [Pesquisar estoque (com e sem informações do modelo)](#pesquisar-estoque)

### Obter todos os itens em estoque
#### Sem informações do produto
```
GET /main/stock
```
Note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:

```json
{
  "_links":{
    "_self": "/main/stock",
    "next": "/main/stock?_page=2"
  },
  "items":[
    {
      "_links":{
        "_self": "/main/stock/2402"
      },
      "id": 2402,
      "modelo": 268,
      "quantidade": 0,
      "tamanho": 40,
      "ativo": 1,
      "barcode": "026840"
    },
  ]
}

```

#### Com informações do produto

Quando o parâmetro _withProducts_ é adicionado à URL, os itens em estoque serão populados com dados dos modelos, exemplo:

```
GET /main/stock?withProducts
```
Irá retornar
```json
{
  "_links":{
    "_self": "/main/stock",
    "next": "/main/stock?_page=2"
  },
  "items":[
    {
      "_links":{
        "_self": "/main/stock//2032"
      },
      "id": 2032,
      "id_modelo": 347,
      "categoria": "Scarpin",
      "subcategoria": "Bico fino ",
      "descricao": "0347",
      "preco_catalogo": 39.99,
      "preco_1": 89.99,
      "preco_2": 62,
      "preco_3": 179.99,
      "producao": "Não definido",
      "tamanho": 37,
      "quantidade": 0,
      "modelo": 347,
      "barcode": "034737"
    },
  ]
}
```

### Obter item em estoque pelo id
```
GET /main/stock/:id
```
Retorna
```json
{
    "_links": {
        "_self": "/main/stock//2402"
    },
    "id": 2402,
    "id_modelo": 268,
    "categoria": "Sapatilhas",
    "subcategoria": "Bico fino ",
    "descricao": null,
    "preco_catalogo": 29.99,
    "preco_1": 49.99,
    "preco_2": 39.99,
    "preco_3": 99.99,
    "producao": "Material espelhado prata \nMaterial \nMaterial ",
    "tamanho": 40,
    "quantidade": 0,
    "modelo": 268,
    "barcode": "026840"
}
```

### Pesquisar estoque
#### Sem informações dos modelos
```
GET /main/stock/search
      ?id={id no estoque}
      &barcode={código de barras}
      &modelo={id do modelo}
      &tamanho={tamanho do modelo}
```
É necessário ao menos um parâmetro,note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:
```json
{
    "_links": {
        "_self": "/main/stock/search?id=1782"
    },
    "items": [
        {
            "_links": {
                "_self": "/main/stock//1782"
            },
            "id": 1782,
            "modelo": 268,
            "quantidade": 0,
            "tamanho": 33,
            "ativo": 1,
            "barcode": "026833"
        }
    ]
}
```
#### Com informações dos modelos
Quando o parâmetro _withProducts_ é adicionado à URL, os itens em estoque serão populados com dados dos modelos, exemplo:
```
GET /main/stock/search
      ?withProducts
      &id={id no estoque}
      &barcode={código de barras}
      &modelo={id do modelo}
      &tamanho={tamanho do modelo}
      &subcategoria={subcategoria do modelo}
      &categoria={categoria do modelo}
      &preco_catalogo={preço de atacado do modelo}
      &preco_1={preço no varejo}
      &preco_2={preço no clube}
      &preco_3={preço promocional}
```
É necessário ao menos um parâmetro,note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:
