import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Subcategories} from './subcategories.model'


class SubcategoriesRouter extends ModelRouter<Subcategories>{

  constructor(){
    super(Subcategories)
  }

  pageSize = 1000

  findByCategory = (req,resp,next) => {
    Subcategories.findByCategory(req.params.cat)
    .then(subcats => super.renderAll(resp,next)(subcats))
    .catch(next)
  }

  applyRoutes(application: restify.Server){

    application.get(`${this.basePath}`,this.findAll)
    application.get(`${this.basePath}/:id`,this.findById)
    application.get(`${this.basePath}/byCategory/:cat`,this.findByCategory)
  }
}


export const subcategoriesRouter = new SubcategoriesRouter()
