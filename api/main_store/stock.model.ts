//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos de pesquisa
import * as joi from 'joi'

const name : string = 'main/stock'
const table : string = 'estoque'

const select_barcode = 'LPAD(CONCAT(modelo,tamanho),6,"0") as `barcode`'
const select_fields = ['estoque.id as id','modelos.id as id_modelo','categorias.nome as categoria','subcategorias.nome as subcategoria','modelos.descricao','preco_catalogo','preco_1','preco_2','preco_3','producao','tamanho','quantidade','modelo']

const SEARCH_PARAMS = joi.object().keys({
  'estoque.id': joi.number(),
  modelo: joi.number(),
  tamanho : joi.number(),
  subcategoria: joi.number(),
  'categorias.id': joi.number(),
  preco_catalogo: joi.number(),
  preco_1: joi.number(),
  preco_2 : joi.number(),
  preco_3: joi.number(),
})

const SEARCH_PARAMS_NO_JOIN = joi.object().keys({
  'estoque.id': joi.number(),
  modelo: joi.number(),
  tamanho : joi.number(),
})



const selectAll = (join = false) => join == true ?  database.getPool()
.select(...select_fields)
.select(database.getPool().raw(select_barcode))
.from(table)
.join('modelos','estoque.modelo','modelos.id')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
.where('ativo','1')
.orderBy('modelos.id','desc')
:
database.getPool()
.select("*")
.select(database.getPool().raw(select_barcode))
.from(table)
.where('ativo','1')
.orderBy('id','desc')

const selectCount = (join = false) => join ?
database.getPool()(table)
.count('modelos.id')
.join('modelos','estoque.modelo','modelos.id')
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
.where('ativo','1')
:
database.getPool()(table)
.count('id')
.where('ativo','1')



const findWhere = (where,select = selectAll(true),join = false) => {
  delete where._page
  if(where.barcode){
    where.modelo = where.barcode.slice(0,-2)
    where.tamanho = where.barcode.slice(-2)
    delete where.barcode
  }
  if(where.id) {
    where['estoque.id'] = where.id
    delete where.id
  }
  if(where.categoria){
    where['categorias.id'] = where.categoria
    delete where.categoria
  }

  const search_params = join ? SEARCH_PARAMS : SEARCH_PARAMS_NO_JOIN

  const {error,value} = joi.validate(where,search_params, {presence: 'optional'})
  if(error) return Promise.reject(error)
  return select.where({...value, ativo: 1})
}


export const Stock = {
  name,

  count: () => selectCount(),
  countWhere: (params,join) => findWhere(params,selectCount(join),join),


  findAll: (join) => selectAll(join),

  findById: (id) => selectAll(true).where('estoque.id',id),

  findByParams: (params,join = false) => findWhere(params,selectAll(join),join),

}
