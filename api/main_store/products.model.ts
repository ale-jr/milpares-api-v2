//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'

//validação de campos de pesquisa
import * as joi from 'joi'


const SEARCH_PARAMS = joi.object().keys({
  'modelos.id': joi.number(),
  subcategoria: joi.number(),
  'categorias.id': joi.number(),
  preco_catalogo: joi.number(),
  preco_1: joi.number(),
  preco_2 : joi.number(),
  preco_3: joi.number(),

})

const name : string = 'products'
const table: string = 'modelos'

const select_fields = ['modelos.id','modelos.nome as nome','categorias.nome as categoria','categorias.id as id_categoria','subcategorias.nome as subcategoria','subcategorias.id as id_subcategoria','modelos.descricao','preco_catalogo','preco_1','preco_2','preco_3','producao',]

const selectAll = () => database.getPool()
.select(...select_fields)
.from(table)
.join('subcategorias','modelos.subcategoria','subcategorias.id')
.join('categorias','subcategorias.categoria','categorias.id')
.orderBy('modelos.id','desc')


const findWhere = (where) => {
  if(where.id) {
    where['modelos.id'] = where.id
    delete where.id
  }
  if(where.categoria){
    where['categorias.id'] = where.categoria
    delete where.categoria
  }
  const {error,value} = joi.validate(where,SEARCH_PARAMS,{presence: 'optional'})
  if(error) return Promise.reject(error)
  return selectAll().where(value)
}



export const Products = {
  name,
  //Contar todos os objetos da tabela
  count: () => database.getPool()(table)
  .count('id'),

  findById : (id) => findWhere({'modelos.id':id}),

  //encontrar dados por parâmetros
  findByParams : (params) => {
    if(params.busca){
      const {busca} = params
      delete params.busca
      console.log(findWhere(params).whereRaw('modelos.nome like "%?%"',[busca]).toString())
      return findWhere(params).where
      ('modelos.nome', 'like', `%${busca}%`)
    }
    return findWhere(params)
  },

  findAll : () => selectAll()
}
