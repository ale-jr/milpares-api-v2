#Categorias e subcategorias
* Categorias
  * [Obter todas categorias](#obter-todas-categorias)
  * [Obter categoria pelo id](#obter-categoria-pelo-id)
* Subcategorias
  * [Obter todas subcategorias](#obter-todas-subcategorias)
  * [Obter subcategorias pelo id](#obter-subcategorias-pelo-id)
  * [Obter subcategorias pelo id da categoria](#obter-subcategorias-pelo-id-da-categoria)
## /categories

### Obter todas categorias
```
GET /categories
```
Retorna

```json
{
  "_links":{
    "_self": "/categories"
  },
  "items":[
  {
    "_links":{
      "_self": "/categories/3"
    },
    "id": 3,
    "nome": "Scarpin",
    "descricao": "",
    "preco_catalogo": 39.99,
    "preco_1": 89.99,
    "preco_2": 62,
    "preco_3": 179.99
  }
  ]
}
```

### Obter categoria pelo id
```
GET /categories/:id
```
Retorna

```json
{
  "_links":{
    "_self": "/categories/4"
  },
  "id": 4,
  "nome": "Botas ",
  "descricao": "",
  "preco_catalogo": 0,
  "preco_1": 0,
  "preco_2": 0,
  "preco_3": 0
}
```

## /subcategories

### Obter todas subcategorias
```
GET /subcategories
```
Retorna

```json
{
  "_links":{
    "_self": "/subcategories"
  },
  "items":[
    {
      "_links":{
        "_self": "/subcategories/1"
      },
      "nome_categoria": "Sapatilhas",
      "nome_subcategoria": "Bico fino ",
      "id": 1,
      "id_categoria": 1
    }
  ]
}
```

### Obter subcategorias pelo id
```
GET /subcategories/:id
```
Retorna

```json
{
  "_links":{
    "_self": "/subcategories/4"
  },
  "nome_categoria": "Scarpin",
  "nome_subcategoria": "Salto Baixo fino ",
  "id": 4,
  "id_categoria": 3
}
```

### Obter subcategorias pelo id da categoria
```
GET /subcategories/byCategory/:id
```
Retorna
```json
{
  "_links":{
    "_self": "undefined"
  },
  "items":[
    {
      "_links":{
        "_self": "/subcategories/7"
      },
      "nome_categoria": "Botas ",
      "nome_subcategoria": "Cano Alto ",
      "id": 7,
      "id_categoria": 4
    }
  ]
}
```
