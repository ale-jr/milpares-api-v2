## /products
Obter e pesquisar modelos na loja principal


* [Obter todos modelos](#obter-todos-modelos)
* [Obter modelos pelo id](#obter-modelos-pelo-id)
* [Pesquisar modelos por parâmetros](#pesquisar-modelos-por-parâmetros)

###Obter todos modelos
```
GET /products
```
Note que podem haver mais páginas, para navegar bastar usar os links de hypermedia \_links.next e \_link.previous. Retorna:

``` json
{
  "_links":{
    "_self": "/products",
    "next": "/products?_page=2"
  },
  "items":[
    {
      "_links":{
        "_self": "/products/348"
      },
      "id": 348,
      "nome": "Sapatilha",
      "categoria": "Sapatilhas",
      "id_categoria": 1,
      "subcategoria": "Bico redondo ",
      "id_subcategoria": 2,
      "descricao": "0348",
      "preco_catalogo": 29.99,
      "preco_1": 49.99,
      "preco_2": 39.99,
      "preco_3": 99.99,
      "producao": "Não definido"
    },
  ]
}
```

### Obter modelos pelo id
```
GET /products/:id
```
Retorna

```json
{
    "_links": {
        "_self": "/products/348"
    },
    "id": 348,
    "nome": "Sapatilha",
    "categoria": "Sapatilhas",
    "id_categoria": 1,
    "subcategoria": "Bico redondo ",
    "id_subcategoria": 2,
    "descricao": "0348",
    "preco_catalogo": 29.99,
    "preco_1": 49.99,
    "preco_2": 39.99,
    "preco_3": 99.99,
    "producao": "Não definido"
}

```

### Pesquisar modelos por parâmetros
```
GET /products/search
    ?busca={busca pelo nome do modelo}
    &id={id do modelo}
    &subcategoria={id da subcategoria do modelo}
    &categoria={id da categoria do modelo}
    &preco_catalogo={preço de atacado do modelo}
    &preco_1={preço no varejo}
    &preco_2={preço no clube}
    &preco_3={preço promocional}
```
Necessário ao menos um parâmetro, retorna
```json
{
  "_links":{
    "_self": "undefined"
  },
  "items":[
    {
      "_links":{
        "_self": "/products/311"
      },
      "id": 311,
      "nome": "Sapatênis",
      "categoria": "Sapatênis ",
      "id_categoria": 9,
      "subcategoria": "Sola tênis ",
      "id_subcategoria": 18,
      "descricao": "0311",
      "preco_catalogo": 39.99,
      "preco_1": 89.99,
      "preco_2": 62,
      "preco_3": 149.99,
      "producao": "Não definido"
    },
  ]
}
```
