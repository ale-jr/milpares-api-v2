//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'


const name : string = 'categories'
const table: string = 'categorias'


export const Categories = {
  name,
  //Contar todos os objetos da tabela
  count: () => database.getPool()(table)
  .count('id'),

  findAll: () => database.getPool()
  .select('*')
  .from(table),

  findById: (id) => database.getPool()
  .select('*')
  .from(table)
  .where('id',id),

  


}
