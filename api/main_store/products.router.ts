import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Products} from './products.model'


class ProductsRouter extends ModelRouter<Products>{

  constructor(){
    super(Products)
  }

  pageSize = 200

  findByParams = (req,resp,next) => {
    Products.findByParams(req.query)
    .then(products => super.renderAll(resp,next)(products))
    .catch(next)
  }

  applyRoutes(application: restify.Server){
    application.get(`${this.basePath}`,this.findAll)
    application.get(`${this.basePath}/:id`,this.findById)
    application.get(`${this.basePath}/search`,this.findByParams)
  }
}


export const productsRouter = new ProductsRouter()
