import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Categories} from './categories.model'


class CategoriesRouter extends ModelRouter<Categories>{

  constructor(){
    super(Categories)
  }

  pageSize = 1000

  applyRoutes(application: restify.Server){

    application.get(`${this.basePath}`,this.findAll)
    application.get(`${this.basePath}/:id`,this.findById)

  }
}



export const categoriesRouter = new CategoriesRouter()
