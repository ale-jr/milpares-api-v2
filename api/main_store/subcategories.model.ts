//variaveis de ambiente
import {environment} from '../../common/environment'

//banco de dados usando knexjs
import {database} from '../../common/database'


const name : string = 'subcategories'
const table: string = 'subcategorias'

const select_fields = ['categorias.nome as nome_categoria','subcategorias.nome as nome_subcategoria','subcategorias.id as id','subcategorias.categoria as id_categoria']

const selectAll = () => database.getPool()
.select(...select_fields)
.from(table)
.join('categorias','subcategorias.categoria','categorias.id')

export const Subcategories = {
  name,

  //Contar todos os objetos da tabela
  count: () => database.getPool()(table)
  .count('subcategorias.id'),

  findAll: () => selectAll(),

  findById: (id) => selectAll().where('subcategorias.id',id),

  findByCategory: (cat) => selectAll().where('subcategorias.categoria',cat)


}
