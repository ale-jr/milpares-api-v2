import {ModelRouter} from '../../common/model-router'
import * as restify from 'restify'
import {NotFoundError} from 'restify-errors'
import {Stock} from './stock.model'


class StockRouter extends ModelRouter<Stock>{

  constructor(){
    super(Stock)
  }

  pageSize = 500

  findByParams = (req,resp,next) => {
    const {query} = req
    let page = parseInt(query._page || 1)
    page = page > 0 ? page : 1

    const skip = (page -1) * this.pageSize

    const join = query.withProducts != undefined ? true : false
    delete query.withProducts
    this.model.countWhere(query,join)
    .then(count => Stock.findByParams(query,join)
    .limit(this.pageSize)
    .offset(skip)
    .then(this.renderAll(resp,next,{
      page,count,pageSize: this.pageSize,url: req.url, resource: '/search'
    })))
    .catch(next)
  }

  findAllJoin = (req,resp,next) => {
    let page = parseInt(req.query._page || 1)
    page = page > 0 ? page : 1

    const skip = (page -1) * this.pageSize


    const join = req.query.withProducts != undefined ? true : false

    this.model.count()
    .then(count=> Stock.findAll(join)
    .limit(this.pageSize)
    .offset(skip)
    .then(this.renderAll(resp,next,{
      page,count, pageSize: this.pageSize,url: req.url
    })))
    .catch(next)
  }

  applyRoutes(application: restify.Server){
    console.log('path',this.basePath)
    application.get(`${this.basePath}`,this.findAllJoin)
    application.get(`${this.basePath}/:id`,this.findById)
    application.get(`${this.basePath}/search`,this.findByParams)
  }
}


export const mainStockRouter = new StockRouter()
