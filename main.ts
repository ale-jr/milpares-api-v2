import { Server } from './server/server'

import { accountsRouter } from './api/accounts/accounts.router'
import { passwordsRouter } from './api/accounts/password.router'

import {stockRouter} from './api/stock/stock.router'

import {ordersRouter} from './api/orders/orders.router'
import {salesRouter} from './api/sales/sales.router'
import {registerRouter} from './api/register/register.router';

import {categoriesRouter} from './api/main_store/categories.router'
import {subcategoriesRouter} from './api/main_store/subcategories.router'
import {productsRouter} from './api/main_store/products.router'
import {mainStockRouter} from './api/main_store/stock.router'



const server = new Server()

server.boostrap([
  accountsRouter,
  passwordsRouter,
  categoriesRouter,
  subcategoriesRouter,
  productsRouter,
  mainStockRouter,
  stockRouter,
  ordersRouter,
  salesRouter,
  registerRouter
]).then(server=>{
  console.log("Server is listening on ",server.application.address())
})
.catch(error=>{
  console.log('Server failed to start')
  console.error(error)
  process.exit(1)
})
