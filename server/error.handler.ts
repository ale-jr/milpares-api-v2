import * as restify from 'restify'

export const handleError = (req:  restify.Request ,resp:restify.Response ,err,done) =>{

  err.toJSON = ()=>{

    return {
      message: err.message
    }
  }

  console.log("error",err)
  switch(err.name){
    case 'MongoError':
    if(err.code === 11000){
      err.statusCode = 400
    }
    break
    case 'ValidationError':
    err.statusCode = 400
    const messages: any[] = []
    for(let name in err.details){
      messages.push({message:err.details[name].message})
    }
    err.toJSON = ()=>({
      errors: messages
    })
    break
  }

  switch(err.sqlState){
    case '23000':
    err.statusCode = 400
    err.toJSON = () => ({
      message: err.sqlMessage
    })
    break
  }
  done()
}
