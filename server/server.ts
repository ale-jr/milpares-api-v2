import * as restify from 'restify'
import {database} from '../common/database'
import {environment} from '../common/environment'
import {logger} from '../common/logger'
import {Router} from '../common/router'
import {mergePatchBodyParser}from './merge-patch.parser'
import {handleError} from './error.handler'
import {tokenParser, tokenParserAdm} from '../security/token.parser'

export class Server {
    application: restify.Server

    initializeDb(): Promise<any> {
      return database.initializeDb()
    }

    initRoutes(routers:Router[]):Promise<any>{
      return new Promise((resolve,reject) => {
        try{
          const options: restify.ServerOptions = {
            name: 'v2-api',
            version: '1.0.0',
            log: logger
          }

          this.application = restify.createServer(options)

          this.application.pre(restify.plugins.requestLogger({
            log:logger
          }))

          this.application.use(restify.plugins.queryParser())

          this.application.use(restify.plugins.bodyParser())

          this.application.use(mergePatchBodyParser)
          
          this.application.use(tokenParser)

          this.application.use(tokenParserAdm)

          for(let router of routers){
            router.applyRoutes(this.application)
          }

          this.application.listen(environment.server.port, () =>{
            resolve(this.application)
          })

          this.application.on('restifyError',handleError)

        }
        catch(error){
          reject(error)
        }
      })
    }

    boostrap(routers: Router[] = []): Promise<Server>{
      return this.initializeDb().then(() => this.initRoutes(routers).then(() => this))

    }

    shutdown(){
      return this.application.close()
    }
}
