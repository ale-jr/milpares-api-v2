import * as sgMail from '@sendgrid/mail'

import {environment} from '../common/environment'

import {Accounts} from '../api/accounts/accounts.model'


sgMail.setApiKey(environment.email.apiKey)

export const newPassword = (email,nome,token) => {
    const url = `http://localhost:3001/${token}`
      const msg = {
        to: email,
        from: 'mailer@milpares.com.br',
        subject: 'Nova senha',
        text: `Olá ${nome} para criar uma nova senha, basta entrar neste link: ${url}`,
      }

      return sgMail.send(msg)

}
