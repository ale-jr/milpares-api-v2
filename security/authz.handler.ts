import * as restify from 'restify'
import {ForbiddenError} from 'restify-errors'

export const authorize =  (req,resp,next) => {
    const requested_id = req.params.id || (req.body ? (req.body.id || false) : false)
    if(req.authenticated !== undefined && req.authenticated.ativo == 1 && (requested_id == false || requested_id == req.authenticated.id)){
      req.params.id = req.authenticated.id
      req.log.debug('User %s is authorized on route %s.',
      req.authenticated.id,
      req.path())
      next()
    }
    else{
      if(req.authenticated){
        req.log.debug('Permission denied for %s. User is blocked, on route %s', req.authenticated.id, req.path())
      }
      next(new ForbiddenError('Permission denied'))
    }
}

export const authorizeAdm = (req,resp,next) => {
  if(req.authenticatedAdm){
      const {authenticatedAdm} = req
    if(Array.isArray(authenticatedAdm.acesso)){
      if(authenticatedAdm.acesso.indexOf(7) < 0) {
        req.log.debug('User blocked on route %s, details %j', req.path(),authenticatedAdm)
        next(new ForbiddenError('Permission denied'))
      }
      else{
        req.log.debug('User %s is authorized on route %s.',
        authenticatedAdm.id,
        req.path())
        next()
      }
    }
  }
  else{
    req.log.debug('Anonimous user blocked on route %s', req.path())
    next(new ForbiddenError('Permission denied'))
  }
}
