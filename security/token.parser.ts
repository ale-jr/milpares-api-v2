import * as restify from 'restify'
import * as jwt from 'jsonwebtoken'
import {Accounts} from '../api/accounts/accounts.model'
import {environment} from '../common/environment'

export const tokenParser: restify.RequestHandler = (req,resp,next) => {
  const token = extractToken(req)
  if(token){
    jwt.verify(token,environment.security.apiSecret,applyBearer(req,next))
  }
  else{
    next()
  }
}

function extractToken(req:restify.Request){
  let token = undefined
  const authorization = req.header('authorization')
  if(authorization){
    const parts: string[] = authorization.split(' ')
    if(parts.length === 2 && parts[0] === 'Bearer'){
      token = parts[1]
    }
  }
  return token
}


function applyBearer (req: restify.Request, next): (error, decoded) => void {
  return (error,decoded) => {
    if(decoded){
      Accounts.findByEmail(decoded.sub)
      .then(user => {
        if(Array.isArray(user)) user = user[0]
        if(user){
            req.authenticated = user
        }
        next()
      })
      .catch(next)
    }
    else{
      next()
    }
  }
}


export const tokenParserAdm: restify.RequestHandler = (req,resp,next) => {
  const token = extractTokenAdm(req)
  if(token){
    jwt.verify(token,environment.security.apiSecretAdm,applyBearerAdm(req,next))
  }
  else{
    next()
  }
}



function extractTokenAdm(req:restify.Request){
  let token = undefined
  const authorization = req.header('authorizationAdm')
  if(authorization){
    const parts: string[] = authorization.split(' ')
    if(parts.length === 2 && parts[0] === 'Bearer'){
      token = parts[1]
    }
  }
  return token
}


function applyBearerAdm (req: restify.Request, next): (error, decoded) => void {
  return (error,decoded) => {
    if(decoded) req.authenticatedAdm = decoded
      next()
  }
}
