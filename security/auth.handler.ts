import * as restify from 'restify'
import * as jwt from 'jsonwebtoken'
import {NotAuthorizedError} from 'restify-errors'
import {Accounts} from '../api/accounts/accounts.model'
import {environment} from '../common/environment'

export const authenticate: restify.RequestHandler = (req,resp,next) => {
  const {email,password} = req.body
  Accounts.findByEmail(email)
  .then(user => {
    if(Array.isArray(user)) user = user[0]
    if(user && Accounts.passwordMatches(user,password)){
      const token = jwt.sign({sub: user.email, iss: 'stores-api'},
      environment.security.apiSecret)
      resp.json({nome:user.nome,email:user.email,accessToken: token})
      return next(false)
    }
    else{
      return next(new NotAuthorizedError('Invalid Credentials'))
    }
  })
  .catch(next)
}
