import {Router} from './router'
import {NotFoundError} from 'restify-errors'

export interface SelectSomethingOptions {
  join?:boolean,
  id?:boolean,
  count?:any,
  select?:any,
  envelope?:{
    envelope?:{
      _self?: boolean
     }
  },
  id_field?: string
}


export abstract class ModelRouter<D extends any> extends Router  {
    basePath: string
    pageSize: number = 4
    constructor(protected model){
      super()
      this.basePath = `/${model.name}`
    }

    protected prepareOne(query){
      return query
    }

    envelope(document: any,options:any = {}): any{
      options = options.envelope || {}

      let resource = Object.assign({_links:{}},document)
      if(options._self != false){
        resource._links._self = `${this.basePath}/${options.resource || ''}${options.resource ? '/' : ''}${resource.id}`
      }


      return resource
    }

    envelopeAll(documents: any, options: any = {}) : any {
      const resource: any = {
        _links: {
          _self: `${options.url}`
        },
        items: documents
      }

      if(options.page && options.count && options.pageSize){
        if(options.page >1){
          resource._links.previous = `${this.basePath}${options.resource || ''}?_page=${options.page -1}`
        }
        //Se for um array
        if(Array.isArray(options.count)){
          //seleciona o primeiro item do array
          options.count = options.count[0]
          options.count = options.count[Object.keys(options.count)[0]]
        }

        const remaining = options.count - (options.page * options.pageSize)
        if(remaining > 0){
          resource._links.next = `${this.basePath}${options.resource || ''}?_page=${options.page+1}`
        }
      }

      return resource
    }

    findAll = (req,resp,next) => {
      let page = parseInt(req.query._page || 1)
      page = page > 0 ? page : 1

      const skip = (page -1) * this.pageSize
      this.model.count()
      .then(count=> this.model.findAll()
      .limit(this.pageSize)
      .offset(skip)
      .then(this.renderAll(resp,next,{
        page,count, pageSize: this.pageSize,url: req.url
      })))
      .catch(next)
    }



    selectSomething = (options: SelectSomethingOptions = {
      join: true,
      id: false,
      count: undefined,
      select: undefined,
      envelope : {},
      id_field: 'id'
    })=>
      (req,resp,next) => {

      if(!options.id_field) options.id_field = 'id'
      
      let page = parseInt(req.query._page || 1)
      page = page > 0 ? page : 1

      const skip = (page -1) * this.pageSize


      const join = req.query.withProducts != undefined ? true : false
      console.log('id_field',options.id_field)
      console.log('id',req.params[options.id_field])
      const id = req.params[options.id_field]

      let count_params = []
      if(options.id) count_params.push(id)
      if(options.join) count_params.push(id)

      let select_params = []

      if(options.join) select_params.push(join)
      if(options.id) select_params.push(id)

      if(!options.count) options.count = () => Promise.resolve()

      options.count(...count_params)
      .then(count=> options.select(...select_params)
      .limit(this.pageSize)
      .offset(skip)
      .then(this.renderAll(resp,next,{ ...options.envelope,
        page,count, pageSize: this.pageSize,url: req.url
      })))
      .catch(next)
    }



    findById = (req,resp,next) =>{
      this.model.findById(req.params.id)
      .then(this.render(resp,next))
      .catch(next)
    }

    save = (req,resp,next) => {
      this.model.save(req.body)
      .then(id => this.model.findById(id))
      .then(this.render(resp,next))
      .catch(next)
    }

    update = (req,resp,next) => {
      this.model.update(req.params.id,req.body)
      .then(id => this.model.findById(req.params.id))
      .then(this.render(resp,next))
      .catch(next)
    }


    delete = (req,resp,next) => {
      this.model.remove(req.params.id)
      .then(result => {
        if(result > 0){
          resp.send(204)
        }
        else{
          throw new NotFoundError('Document not Found')
        }
        return next()
      })
      .catch(next)
    }


}
