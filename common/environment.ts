export const environment = {
  server: {
    port: process.env.SERVER_PORT || 3001
  },
  db: {
    connectionLimit: process.env.CONN_LIMIT || 10,
    host     : process.env.DB_HOST ||'localhost',
    port     : process.env.DB_PORT ||'3306',
    user     : process.env.DB_USER ||'root',
    password : process.env.DB_PWD ||"root",
    database : process.env.DB_DATABASE || 'loja',
    multipleStatements: true
  },
  security: {
    saltRounds: process.env.SALT_ROUNDS || 10,
    apiSecret: process.env.API_SECRET || 'secret',
    apiSecretAdm: process.env.ADM_SECRET || "Ahphi4Eipaic2niechasau5aekuete4Ir9oochioGu0baephae4eesh3uNgoongae2uT0dadohLoh5phel1iezozaime8iashesoi2ieThiingeci0DiH8ohboh9loxo0ahr4coh5eezeevairiehai0ahdee2mafa1bei4Yuquai7iethoshah6vahquohbaivei1Oh",
    token_timeout:  process.env.TOKEN_TIMEOUT || 24 * 60 * 60 * 1000 //tempo em ms (24 horas)
  },
  log:{
    level: process.env.LOG_LEVEL || 'debug',
    name: 'v2-api-logger'
  },
  email: {
    apiKey: process.env.SG_KEY || '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',
  }
}
