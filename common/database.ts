import * as _knex from 'knex'
import {environment} from '../common/environment'


let knex = undefined
const  knexConnect = () => {
  knex = _knex({
  client: 'mysql',
  connection: environment.db,
  pool: {min: 0, max: 7}
})
return knex
}

export const database = {
  initializeDb: () => {
      return new Promise((resolve, reject) => {
        try{
          if(knex) return resolve(knex)
          knexConnect()
          resolve(knex)
        }
        catch(error){
          reject(error)
        }
      })
    },
  getPool: () => knex == undefined ? knexConnect() : knex

}
