import * as restify from 'restify'
import {EventEmitter} from 'events'
import {NotFoundError} from 'restify-errors'

export abstract class Router extends EventEmitter {
  abstract applyRoutes(application: restify.Server)

  envelope(document : any,options = {}) : any {
    return document
  }

  envelopeAll(documents: any, options: any = {}) : any {
    return documents
  }

  render(response: restify.Response, next: restify.Next){
    return(document) => {
      if(Array.isArray(document)){
        if(document.length > 0){
          document = document[0]
        }
        else{
          throw new NotFoundError('Documento não encontrado')
        }
      }

      if(document){
        this.emit('beforeRender',document)
        response.json(this.envelope(document))
      }
      else{
        throw new NotFoundError('Documento não encontrado')
      }
      return next()
    }
  }

  renderAll(response: restify.Response, next: restify.Next, options: any = {}){
    return (documents:any[]) => {
      if(documents){
        documents.forEach((document,index,array) => {
          this.emit('beforeRender',document)
          array[index] = this.envelope(document,options)
        })
        response.json(this.envelopeAll(documents,options))
      }
      else{
        response.json(this.envelopeAll([]))
      }
      return next()
    }
  }

}
